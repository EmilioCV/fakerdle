const WORD = "FAKER";
const board = document.getElementById('game-board');
const keyboard = document.getElementById('keyboard');
const keys = "QWERTYUIOPASDFGHJKLZXCVBNM";

let VALID_WORDS;
let currentRow = [];
let rowIndex = 0;
let rowElements = [];
let gameOver = false;

const modal = document.getElementById("modal");
const closeModal = document.getElementById("close-modal");
const modalMessage = document.getElementById("modal-message");
const errorMessage = document.getElementById("error-message");

document.addEventListener('DOMContentLoaded', () => {
    loadWordList();
});

async function loadWordList() {
    try {
        const response = await fetch('sgb-words.txt');
        let text = await response.text();
        VALID_WORDS = text.split('\n').map(word => word.trim()).filter(word => word.length > 0);
        VALID_WORDS = VALID_WORDS.map(function(word){ return word.toUpperCase();})
    } catch (error) {
        console.error('Error loading word list:', error);
    }
}
closeModal.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

for (let i = 0; i < 6; i++) {
    const row = [];
    for (let j = 0; j < 5; j++) {
        const cell = document.createElement('div');
        board.appendChild(cell);
        row.push(cell);
    }
    rowElements.push(row);
}

keys.split('').forEach(key => {
    const button = document.createElement('button');
    button.textContent = key;
    button.onclick = () => handleKeyClick(key);
    keyboard.appendChild(button);
});

const enterButton = document.createElement('button');
enterButton.textContent = '↵';
enterButton.onclick = handleEnter;
keyboard.appendChild(enterButton);

const deleteButton = document.createElement('button');
deleteButton.textContent = '⌫';
deleteButton.onclick = handleDelete;
keyboard.appendChild(deleteButton);

function handleKeyClick(key) {
    if (gameOver) return;
    if (currentRow.length < 5) {
        currentRow.push(key);
        updateBoard();
    }
}

function handleEnter() {
    if (gameOver) return;
    if (currentRow.length === 5) {
        const guess = currentRow.join('');
        if (VALID_WORDS.includes(guess)) {
            checkRow();
            if (!gameOver) {
                currentRow = [];
                rowIndex++;
                errorMessage.textContent = '';
            }
        } else {
            showError("This word isn't valid");
            shakeRow();
        }
    }
}

function handleDelete() {
    if (gameOver) return;
    currentRow.pop();
    updateBoard();
}

function updateBoard() {
    rowElements[rowIndex].forEach((cell, j) => {
        cell.textContent = currentRow[j] || '';
    });
}

function checkRow() {
    let correctCount = 0;
    currentRow.forEach((letter, i) => {
        const cell = rowElements[rowIndex][i];
        if (letter === WORD[i]) {
            cell.classList.add('correct');
            correctCount++;
        } else if (WORD.includes(letter)) {
            cell.classList.add('present');
        } else {
            cell.classList.add('absent');
        }
    });
    if (correctCount === 5) {
        gameOver = true;
        errorMessage.textContent = '';
        showModal("Congratulations! You've guessed the word!");
    } else if (rowIndex === 5) {
        gameOver = true;
        showModal("Game over! The word was FAKER.");
    }
}

function showModal(message) {
    modalMessage.textContent = message;
    modal.style.display = "block";
}

function showError(message) {
    errorMessage.textContent = message;
}

function shakeRow() {
    const currentRowElement = rowElements[rowIndex];
    currentRowElement.forEach(cell => {
        cell.classList.add('shake');
        cell.addEventListener('animationend', () => {
            cell.classList.remove('shake');
        }, { once: true });
    });
}

updateBoard();
